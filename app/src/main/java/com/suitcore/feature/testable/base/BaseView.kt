package com.suitcore.feature.testable.base

/**
 * Created by AldioFirando on 18/05/2021.
 */
interface BaseView<T> {
    fun setPresenter(presenter: T)
}