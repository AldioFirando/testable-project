package com.suitcore.feature.testable.contract

import com.suitcore.data.model.User
import com.suitcore.feature.testable.base.BasePresenter
import com.suitcore.feature.testable.base.BaseView
import io.realm.RealmResults

/**
 * Created by AldioFirando on 18/05/2021.
 */

class MemberContract {

    interface View : BaseView<Presenter?> {
        fun setItemToView(members: List<User>?)
        fun setNoDataLoadToView()
        fun setErrorToView(error: Any?)
    }

    interface Presenter : BasePresenter {
        fun loadData()
    }
}