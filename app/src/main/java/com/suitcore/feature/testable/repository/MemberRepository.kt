package com.suitcore.feature.testable.repository

import com.suitcore.data.model.User
import com.suitcore.feature.testable.repository.MemberDataSource.LoadDataCallback
import io.realm.RealmResults

/**
 * Created by AldioFirando on 18/05/2021.
 */
class MemberRepository(val remoteDataSource: MemberDataSource) : MemberDataSource {

    override fun getMember(callback: LoadDataCallback?) {
        remoteDataSource.getMember(object: LoadDataCallback{
            override fun onMemberCacheLoaded(members: RealmResults<User>?) {
            }

            override fun onMemberLoaded(members: List<User>?) {
                callback?.onMemberLoaded(members)
            }

            override fun onMemberEmpty() {
                callback?.onMemberEmpty()
            }

            override fun onFailed(error: Any?) {
                callback?.onFailed(error)
            }

        })
    }
}