package com.suitcore.feature.testable.base

/**
 * Created by AldioFirando on 18/05/2021.
 */
interface BasePresenter {
    fun start()
}