package com.suitcore.feature.testable

import com.suitcore.feature.testable.contract.MemberContract
import com.suitcore.feature.testable.repository.MemberDataSource
import com.suitcore.feature.testable.repository.MemberRepository
import io.realm.RealmResults

/**
 * Created by AldioFirando on 18/05/2021.
 */
class MemberPresenter(val view: MemberContract.View, val repository: MemberRepository?) : MemberContract.Presenter {

    override fun loadData() {
        repository?.getMember(object : MemberDataSource.LoadDataCallback {
            override fun onMemberCacheLoaded(members: RealmResults<com.suitcore.data.model.User>?) {

            }

            override fun onMemberLoaded(members: List<com.suitcore.data.model.User>?) {
                view.setItemToView(members)
            }

            override fun onMemberEmpty() {
                view.setNoDataLoadToView()
            }

            override fun onFailed(error: Any?) {
                view.setErrorToView(error)
            }
        })
    }

    override fun start() {
        loadData()
    }


}