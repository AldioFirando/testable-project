package com.suitcore.feature.testable.repository

import com.suitcore.R
import com.suitcore.data.remote.services.APIService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by AldioFirando on 18/05/2021.
 */
class MemberRemoteDataSource(service: APIService) : MemberDataSource {

    private val service: APIService
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

    init {
        this.service = service
    }

    override fun getMember(callback: MemberDataSource.LoadDataCallback?) {
        mCompositeDisposable?.add(
                service.getMembers(10, 1)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ data ->
                            if (data != null) {
                                if (data.arrayData?.isNotEmpty()!!) {
                                    callback?.onMemberLoaded(data.arrayData!!)
                                } else {
                                    callback?.onMemberEmpty()
                                }
                            } else {
                                callback?.onFailed(R.string.txt_error_global)
                            }
                        }, {
                            callback?.onFailed(it)
                        })
        )
    }
}