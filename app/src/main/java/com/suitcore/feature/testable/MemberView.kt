package com.suitcore.feature.testable

import com.suitcore.base.presenter.MvpView
import com.suitcore.data.model.User
import io.realm.RealmList
import io.realm.RealmResults

/**
 * Created by AldioFirando on 18/05/2021.
 */
interface MemberView : MvpView {

    fun onMemberCacheLoaded(members: RealmResults<User>?)

    fun onMemberLoaded(members: List<User>?)

    fun onMemberEmpty()

    fun onFailed(error: Any?)

}