package com.suitcore.feature.testable.repository

import com.suitcore.data.model.User
import io.realm.RealmResults

/**
 * Created by AldioFirando on 18/05/2021.
 */
interface MemberDataSource {

    fun getMember(callback: LoadDataCallback?)

    interface LoadDataCallback {
        fun onMemberCacheLoaded(members: RealmResults<User>?)
        fun onMemberLoaded(members: List<User>?)
        fun onMemberEmpty()
        fun onFailed(error: Any?)
    }
}