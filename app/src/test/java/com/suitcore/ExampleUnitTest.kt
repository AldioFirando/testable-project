package com.suitcore

import com.suitcore.data.model.User
import com.suitcore.feature.testable.MemberPresenter
import com.suitcore.feature.testable.contract.MemberContract
import com.suitcore.feature.testable.repository.MemberDataSource
import com.suitcore.feature.testable.repository.MemberRepository
import org.junit.Before
import org.junit.Test
import org.mockito.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    private val ITEMS: List<User> = emptyList<User>()
    private val THROWABLE = Throwable()

    @Mock
    var repository: MemberRepository? = null

    @Mock
    var view: MemberContract.View? = null

    private var presenter: MemberPresenter? = null

    @Captor
    private val loadCallbackArgumentCaptor: ArgumentCaptor<MemberDataSource.LoadDataCallback>? = null

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = MemberPresenter(view!!, repository)
    }

    @Test
    fun initPresenterToViewTest() {
        presenter = MemberPresenter(view!!, repository)
        Mockito.verify(view)?.setPresenter(presenter)
    }

    @Test
    fun loadDataToViewTest() {
        presenter?.loadData()
        Mockito.verify(repository)?.getMember(loadCallbackArgumentCaptor!!.capture())
        loadCallbackArgumentCaptor?.value?.onMemberLoaded(ITEMS)
        Mockito.verify(view)?.setItemToView(ITEMS)
    }

    @Test
    fun loadErrorToViewTest() {
        presenter?.start()
        Mockito.verify(repository)?.getMember(loadCallbackArgumentCaptor!!.capture())
        loadCallbackArgumentCaptor?.value?.onFailed(THROWABLE)
        Mockito.verify(view)?.setErrorToView(THROWABLE)
    }
}
